package com.bank.bincode.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bank.bincode.model.Bank;
import com.bank.bincode.service.BankService;
import com.bank.bincode.utils.BeanUtil;
import com.bank.bincode.utils.JsonResultUtil;


/******************************************************************************************************************
 *
 * 银行卡信息查询
 * 
 * @author junrong
 * @date 2016年8月17日 上午9:16:39
 * @version 1.0
 *****************************************************************************************************************/
@Controller("bankController")
@RequestMapping("/bank")
public class BankController {

	
	@Autowired
	private BankService bankService;

	/**
	 * 根据银行卡查询银行卡信息
	 * @param token 用户token
	 * @param card 银行卡号
	 * @return
	 */
	@ResponseBody
	@RequestMapping
	public Map<String, Object> find(@RequestParam(required = true) String card) {
		
		Bank bank = bankService.getBankByCard(card);
		if(bank == null){
			bank = new Bank();
		}
		return JsonResultUtil.getSuccessResult(BeanUtil.transBean2Map(bank));
	}
}
