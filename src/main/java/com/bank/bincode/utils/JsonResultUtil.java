package com.bank.bincode.utils;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.bank.bincode.constants.Constants;
import com.bank.bincode.constants.Constants.ResultCode;

/******************************************************************************************************************
*
* 
* 
* @author junrong
* @date 2016年8月4日 下午4:46:02
* @version 1.0
*****************************************************************************************************************/
public class JsonResultUtil {

	
	/**
	 * 获取服务器错误的jsonresult map
	 * @param resultMsg
	 * @return
	 */
	public static Map<String, Object> getServerErrorResult(String resultMsg){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.SERVERERROR);
		result.put(Constants.RESULTMSG, resultMsg==null?"":resultMsg);
		return result;
	}
	
	/**
	 * 获取请求参数错误的jsonresult map
	 * @param resultMsg
	 * @return
	 */
	public static Map<String, Object> getParamErrorExceptionResult(String resultMsg){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.PARAMERROR);
		result.put(Constants.RESULTMSG, resultMsg==null?"":resultMsg);
		return result;		
	}
	/**
	 * 封装请求参数错误描述的map（include errorCode）
	 * @param errorCode
	 * @param resultMsg
	 * @return
	 */
	public static Map<String, Object> getParamErrorResult(String errorCode, String resultMsg){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.PARAMERROR);
		result.put(Constants.ERRORCODE	,errorCode!=null?errorCode:"");
		result.put(Constants.RESULTMSG, resultMsg==null?"":resultMsg);
		return result;		
	}
	/**
	 * 封装参数错误描述信息
	 * 
	 * @param resultObj
	 * @param resultMsg
	 * @return
	 */
	public static Map<String, Object> getParamErrorResult(Map<String,Object> resultObj, String resultMsg){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.PARAMERROR);
		result.put(Constants.RESULTMSG, resultMsg==null?"":resultMsg);
		if (resultObj!=null) {
			result.put(Constants.RESULT, resultObj);
		}
		return result;		
	}
	
	/**
	 * 获取token错误的jsonresult map
	 * @return
	 */
	public static Map<String, Object> getTokenErrorResult(){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.TOKENERROE);
		result.put(Constants.RESULTMSG, "用户登录信息已过期");
		return result;		
	}
	
	/**
	 * 获取token过期的jsonresult map
	 * @return
	 */
	public static Map<String, Object> getTokenExpiredResult(){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.TOKENERROE);
		result.put(Constants.RESULTMSG, "用户登录信息已过期");
		return result;		
	}
	
	public static Map<String, Object> getTokenEmptyResult(){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE, ResultCode.TOKENERROE);
		result.put(Constants.RESULTMSG, "token不能为空");
		return result;
	} 
	
	/** 
	 * 获取请求操作成功的jsonresult map
	 * @param resultMsg
	 * @param resultObject
	 * @return
	 */
	public static Map<String, Object> getSuccessResult(String resultMsg,Map<String, Object> resultObject){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.SUCCESS);
		result.put(Constants.RESULTMSG, resultMsg==null?"":resultMsg);
		if (resultObject == null) {
			resultObject = new HashMap<String, Object>();
		}
		result.put(Constants.RESULT, resultObject);
		return result;	
	}
	public static Map<String, Object> getSuccessResult(String resultMsg){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.SUCCESS);
		result.put(Constants.RESULTMSG, resultMsg==null?"":resultMsg);
		result.put(Constants.RESULT,new HashMap<String, Object>());
		return result;	
	}
	/** 
	 * 获取请求操作成功的jsonresult map
	 * @param resultMsg
	 * @param resultObject 
	 * @return
	 */
	public static Map<String, Object> getSuccessResultForObject(String resultMsg,Object resultObject){
		Map<String, Object> result = new HashMap<String, Object>();
		result.put(Constants.RESULTCODE	,ResultCode.SUCCESS);
		result.put(Constants.RESULTMSG, resultMsg==null?"":resultMsg); 
		if (resultObject == null) {
			resultObject = new HashMap<String, Object>();
		}else {
			resultObject = JSON.toJSON(resultObject);
		}
		result.put(Constants.RESULT, resultObject);
		return result;	
	}
	/**
	 * 获取请求操作成功的jsonresult map
	 * @param resultObject
	 * @return
	 */
	public static Map<String, Object> getSuccessResult(Map<String, Object> resultObject){
		return getSuccessResult("请求操作成功", resultObject);
	}
	
	/**
	 * 获取请求操作成功的jsonresult map
	 * @return
	 */
	public static Map<String, Object> getSuccessResult(){
		return getSuccessResult("请求操作成功", new HashMap<String, Object>());
	}
	
}
